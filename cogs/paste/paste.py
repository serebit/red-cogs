import requests
from requests.exceptions import ConnectionError
import re
import json

from discord.ext import commands



def setup(bot):
    cog = PasteCog(bot)
    bot.add_listener(cog.on_message, "on_message")
    bot.add_cog(cog)


class PasteCog:
    def __init__(self, bot):
        self.bot = bot

    @commands.command(pass_context=True)
    async def paste(self, ctx, *, code: str):
        if not ctx.message.channel.is_private:
            await self.bot.delete_message(ctx.message)
        response = await paste(ctx.message.author, code)
        await self.bot.say(response)
        
    async def on_message(self, message):
        self.bot.say("Hello!")
        matches = re.match("(?:```(.+)?\n((?:.*\n)+?)```)+", message.content, re.MULTILINE)
        if matches is not None:
            print(matches)
            for i in range(1, len(matches.groups())):
                lang = matches.group(i) or "text"
                code = matches.group(i + 1)
                if exceeds_length_limit(code):
                    response = await paste(message.author, lang, code)
                    if message in self.bot.messages:
                        self.bot.delete_message(message)
                    self.bot.say(response)
        
        
async def paste(author, code: str) -> str:
    host = "https://hastebin.com"
    path = "/documents"
    try:
        response = requests.post(
            host + path,
            data = code,
            headers = {
                "content-type": "text/plain",
                "User-Agent": "SB-5677/0.0.1"
            },
            verify = True
        )
        if response.status_code == 200:
            paste_url = host + "/" + json.loads(response.text)["key"]
            print("Created paste at " + paste_url)
            return "Paste by " + author.mention + ": <" + paste_url + ">"
        else:
            print(
                "The request to "
                + host + path +
                " returned "
                + str(response.status_code) +
                "."
            )
            return (
                "There was an error creating "
                + author.mention +
                "\'s paste. Check the console for details."
            )
    except ConnectionError:
        print("The connection to " + host + "failed.")
        return "The connection to " + host + "failed."


def exceeds_length_limit(code: str) -> bool:
    char_limit = 256
    char_count = len(code)
    line_limit = 10
    line_count = len(code.split("\n"))
    print("Char count: " + str(char_count))
    print("Line count: " + str(line_count))
    return char_count > char_limit or line_count > line_limit
