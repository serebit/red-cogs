from discord.ext import commands


def setup(bot):
    bot.add_cog(TitanCog(bot))


class TitanCog:
    def __init__(self, bot):
        self.bot = bot

    @commands.command()
    async def identify(self):
        response = await identify(self)
        await self.bot.say(response)

    @commands.command()
    async def pilot(self):
        response = await pilot(self)
        await self.bot.say(response)

    @commands.command()
    async def protocols(self):
        response = await protocols()
        await self.bot.say(response)


async def identify(titan: TitanCog) -> str:
    app_info = await titan.bot.application_info()
    name = app_info.name
    nickname = name[:2]
    return (
        "I am `"
        + name +
        "`, a Vanguard-class Titan of the Militia SRS, Marauder Corps. You may call me `"
        + nickname +
        "`."
    )


async def pilot(titan: TitanCog) -> str:
    owner = await titan.bot.get_user_info(titan.bot.settings.owner)
    response = "I am linked to Pilot `" + str(owner) + "`."
    return response


async def protocols() -> str:
    return (
        "```yaml\n" +
        "Protocol 1: Link to Pilot\n" +
        "Protocol 2: Uphold the Mission\n" +
        "Protocol 3: Protect the Pilot\n" +
        "```"
    )
