import json
import os

from __main__ import send_cmd_help
from cogs.utils import checks
from discord.ext import commands

dataDirectory = "data/roleplay/"
characterDataFile = "data/roleplay/characters.json"


def setup(bot):
    bot.add_cog(RoleplayCog(bot))


class RoleplayCog:
    def __init__(self, bot):
        self.bot = bot

    @commands.group(aliases=["char"], pass_context=True, no_pm=True)
    async def character(self, ctx):
        if ctx.invoked_subcommand is None:
            await send_cmd_help(ctx)

    @character.group(name="add", pass_context=True)
    @checks.mod_or_permissions(moderator=True)
    async def char_add(self, ctx, name: str, link: str):
        response = await Character.add(ctx, name, link)
        await self.bot.say(response)

    @character.group(name="list", pass_context=True)
    async def char_list(self, ctx):
        response = await Character.list(ctx)
        await self.bot.say(response)

    @character.group(name="view", pass_context=True)
    async def char_view(self, ctx, name: str):
        response = await Character.view(ctx, name)
        await self.bot.say(response)

    @character.group(name="edit", pass_context=True)
    @checks.mod_or_permissions(moderator=True)
    async def char_edit(self, ctx, name: str, link: str):
        response = await Character.edit(ctx, name, link)
        await self.bot.say(response)

    @character.group(name="remove", pass_context=True)
    @checks.mod_or_permissions(moderator=True)
    async def char_remove(self, ctx, name: str):
        response = await Character.remove(ctx, name)
        await self.bot.say(response)


class Character(object):
    @staticmethod
    async def add(ctx, name: str, link: str) -> str:
        server = ctx.message.server
        characters = read_data(characterDataFile)
        if server.id not in characters:
            characters[server.id] = {}
        character_list = characters[server.id]
        if name not in character_list:
            character_list[name] = link
            characters[server.id] = character_list
            write_data(characters, characterDataFile)
            return "Character successfully added."
        else:
            return "This character already exists. Use `edit` to edit it."

    @staticmethod
    async def list(ctx) -> str:
        server = ctx.message.server
        characters = read_data(characterDataFile)
        if server.id not in characters:
            characters[server.id] = {}
        character_list = characters[server.id]
        if bool(character_list):
            response = "**Characters:**\n\n"
            for key in character_list:
                response += key + ": <" + character_list[key] + ">\n"
            return response
        else:
            return "No characters exist yet. Use `add` to add one."

    @staticmethod
    async def view(ctx, name: str) -> str:
        server = ctx.message.server
        characters = read_data(characterDataFile)
        if server.id not in characters:
            characters[server.id] = {}
        character_list = characters[server.id]
        if name in character_list:
            return name + ": <" + character_list[name] + ">"
        else:
            return (
                "No character named "
                + name +
                "exists. Use `add` to add it."
            )

    @staticmethod
    async def edit(ctx, name: str, link: str) -> str:
        server = ctx.message.server
        characters = read_data(characterDataFile)
        if server.id not in characters:
            characters[server.id] = {}
        character_list = characters[server.id]
        if name in character_list:
            character_list[name] = link
            characters[server.id] = character_list
            write_data(characters, characterDataFile)
            return "Character successfully edited."
        else:
            return "This character does not exist. Use `add` to add it."

    @staticmethod
    async def remove(ctx, name: str) -> str:
        server = ctx.message.server
        characters = read_data(characterDataFile)
        if server.id not in characters:
            characters[server.id] = {}
        character_list = characters[server.id]
        if name in character_list:
            del character_list[name]
            characters[server.id] = character_list
            write_data(characters, characterDataFile)
            return "Character successfully removed."
        else:
            return "This character does not exist. Use `add` to add it."


def read_data(data_file) -> dict:
    if not os.path.exists(dataDirectory):
        os.mkdir(dataDirectory)
    if not os.path.isfile(data_file):
        write_data({}, data_file)
    with open(data_file, 'r') as file:
        return json.loads(file.read())


def get_file_size(data_file) -> int:
    return os.path.getsize(data_file)


def write_data(data, data_file):
    with open(data_file, 'w+') as f:
        json.dump(data, f)
